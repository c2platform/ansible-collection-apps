# CHANGELOG

## 1.0.3 ( )

* [`guacamole`](./roles/guacamole) new variable
  `guacamole_docker_initdb_sql_cmd` with updated argument, update for
  `initdb.sh` `--postgresql` (  `--postgres` no longer works).

## 1.0.2 ( 2022-10-1 )

* [desktop](./roles/desktop) Defaults vars `desktop_files`, `desktop_directories`, `desktop_acl`, `desktop_unarchive` and `desktop_disabled` for [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).
* [guacamole](./roles/guacamole) Defaults vars `guacamole_files`, `guacamole_directories`, `guacamole_acl`, `guacamole_unarchive` and `guacamole_disabled` for [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).

## 1.0.0 ( 2022-09-14 )

* Initial release
