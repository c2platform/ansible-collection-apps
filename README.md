# Ansible Collection - c2platform.apps

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-apps/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-apps/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/apps/)

C2 Platform applications such as Guacamole, Nextcloud, Jenkins, Nexus, SonarQube

## Roles

* [desktop](./roles/desktop) create a Docker based desktop using [onknows/desktop](https://hub.docker.com/r/onknows/desktop)
* [guacamole](./roles/guacamole) install [Apache Guacamole](https://guacamole.apache.org/) remote desktop gateway for example for [onknows/desktop](https://hub.docker.com/r/onknows/desktop)
* [jenkins](./roles/jenkins)
* [nextcloud](./roles/nextcloud) install [Nextcloud](https://nextcloud.com/).
* [nexus](./roles/nexus) install [Nexus Repository](https://www.sonatype.com/products/repository-pro).

## Plugins
