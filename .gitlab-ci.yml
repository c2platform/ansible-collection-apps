---
default:
  image: registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.1

before_script:
  - source ~/.bashrc
  - python3 --version
  - pip3 --version
  - ansible --version
  - ansible-lint --version
  - yamllint --version

workflow:  # run the pipeline only on MRs and default branch
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - linters
  - galaxy
  - release

prepare:
  stage: build
  script:
    - |
      C2_VERSION=$(cat galaxy.yml | grep -Po "version: \K[0-9.]+")
      C2_NAME=$(cat galaxy.yml | grep -Po "name: \K\w+")
      C2_NAMESPACE=$(cat galaxy.yml | grep -Po "namespace: \K\w+")
    - 'echo "C2_VERSION: $C2_VERSION, C2_NAME: $C2_NAME, C2_NAMESPACE: $C2_NAMESPACE"'
    - |
      echo "C2_VERSION=$C2_VERSION" >> variables.env
      echo "C2_NAME=$C2_NAME" >> variables.env
      echo "C2_NAMESPACE=$C2_NAMESPACE" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env

build:
  stage: build
  needs: [prepare]
  script:
    - cat README-GALAXY.md > README.md  # readme property in galaxy.yml is ignored by galaxy website
    - ansible-galaxy collection build . --force
  artifacts:
    paths:
      - $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz

yamllint:
  stage: linters
  script:
    - yamllint -c .yamllint .

ansible-lint:
  stage: linters
  needs: [prepare, build]
  script:
    - ansible-galaxy collection install $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz
    - ansible-lint -c .ansible-lint

publish:
  stage: galaxy
  needs: [prepare, build, yamllint, ansible-lint]
  script:
    - cat README-GALAXY.md > README.md  # readme property in galaxy.yml is ignored by galaxy website
    - ansible-galaxy collection build . --force
    - ansible-galaxy collection publish $C2_NAMESPACE-$C2_NAME-$C2_VERSION.tar.gz --api-key $GALAXY_API_KEY
  when: manual

gitlab-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: [publish, prepare]
  before_script: []
  script:
    - echo "Create release for $C2_NAMESPACE.$C2_NAME $C2_VERSION"
  release:
    name: $C2_NAMESPACE.$C2_NAME $C2_VERSION
    description: './CHANGELOG.md'
    tag_name: $C2_VERSION
    ref: $CI_COMMIT_SHA
    assets:
      links:
        - name: $C2_NAMESPACE.$C2_NAME
          url: https://galaxy.ansible.com/$C2_NAMESPACE/$C2_NAME
