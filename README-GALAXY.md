# Ansible Collection - c2platform.apps

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-apps/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-apps/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-apps/-/pipelines)

See [README](https://gitlab.com/c2platform/ansible-collection-apps/-/blob/master/README.md).
